//
//  main.m
//  RadiusProject
//
//  Created by AMK on 11/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
