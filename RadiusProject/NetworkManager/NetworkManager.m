//
//  NetworkManager.m
//  RadiusProject
//
//  Created by AMK on 17/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "NetworkManager.h"

@implementation NetworkManager

+ (instancetype)defaultManager{
    
    static NetworkManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self class] new];
    });
    return sharedManager;
}

- (void)downloadFaclilityData:(void (^)(id responseObject , NSError *error))completionBlock{
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://my-json-server.typicode.com/iranjith4/ad-assignment/db"]];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSData * responseData = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
            if(completionBlock) completionBlock(jsonDict,nil);
    }] resume];
}

@end
