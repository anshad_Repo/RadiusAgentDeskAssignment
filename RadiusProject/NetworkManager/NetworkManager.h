//
//  NetworkManager.h
//  RadiusProject
//
//  Created by AMK on 17/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

+ (instancetype)defaultManager;

- (void)downloadFaclilityData:(void(^)(id responseObject, NSError *error))completionBlock;

@end
