//
//  ExclusionPair+CoreDataProperties.h
//  
//
//  Created by AMK on 19/08/18.
//
//

#import "ExclusionPair+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ExclusionPair (CoreDataProperties)

+ (NSFetchRequest<ExclusionPair *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *attribute;
@property (nullable, nonatomic, retain) NSObject *attribute1;
@property (nullable, nonatomic, retain) NSSet<FacilityOptionTuple *> *mutualExclusionPair;

@end

@interface ExclusionPair (CoreDataGeneratedAccessors)

- (void)addMutualExclusionPairObject:(FacilityOptionTuple *)value;
- (void)removeMutualExclusionPairObject:(FacilityOptionTuple *)value;
- (void)addMutualExclusionPair:(NSSet<FacilityOptionTuple *> *)values;
- (void)removeMutualExclusionPair:(NSSet<FacilityOptionTuple *> *)values;

@end

NS_ASSUME_NONNULL_END
