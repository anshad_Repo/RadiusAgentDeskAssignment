//
//  ExclusionPair+CoreDataClass.h
//  
//
//  Created by AMK on 19/08/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FacilityOptionTuple;

NS_ASSUME_NONNULL_BEGIN

@interface ExclusionPair : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "ExclusionPair+CoreDataProperties.h"
