//
//  ExclusionPair+CoreDataProperties.m
//  
//
//  Created by AMK on 19/08/18.
//
//

#import "ExclusionPair+CoreDataProperties.h"

@implementation ExclusionPair (CoreDataProperties)

+ (NSFetchRequest<ExclusionPair *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ExclusionPair"];
}

@dynamic attribute;
@dynamic attribute1;
@dynamic mutualExclusionPair;

@end
