//
//  BaseModel+CoreDataProperties.h
//  
//
//  Created by AMK on 21/08/18.
//
//

#import "BaseModel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BaseModel (CoreDataProperties)

+ (NSFetchRequest<BaseModel *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *identifier;

@end

NS_ASSUME_NONNULL_END
