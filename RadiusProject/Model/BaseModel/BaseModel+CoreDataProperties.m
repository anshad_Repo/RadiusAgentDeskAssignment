//
//  BaseModel+CoreDataProperties.m
//  
//
//  Created by AMK on 21/08/18.
//
//

#import "BaseModel+CoreDataProperties.h"

@implementation BaseModel (CoreDataProperties)

+ (NSFetchRequest<BaseModel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"BaseModel"];
}

@dynamic identifier;

@end
