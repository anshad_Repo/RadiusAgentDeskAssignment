//
//  BaseModel+CoreDataClass.h
//  
//
//  Created by AMK on 21/08/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseModel : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BaseModel+CoreDataProperties.h"
