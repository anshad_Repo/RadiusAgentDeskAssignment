//
//  Facility+CoreDataProperties.h
//  
//
//  Created by AMK on 19/08/18.
//
//

#import "Facility+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Facility (CoreDataProperties)

+ (NSFetchRequest<Facility *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, retain) NSSet<Option *> *options;

@end

@interface Facility (CoreDataGeneratedAccessors)

- (void)addOptionsObject:(Option *)value;
- (void)removeOptionsObject:(Option *)value;
- (void)addOptions:(NSSet<Option *> *)values;
- (void)removeOptions:(NSSet<Option *> *)values;

@end

NS_ASSUME_NONNULL_END
