//
//  Facility+CoreDataClass.h
//  
//
//  Created by AMK on 19/08/18.
//
//

#import <Foundation/Foundation.h>
#import "BaseModel+CoreDataClass.h"

@class Option;

NS_ASSUME_NONNULL_BEGIN

@interface Facility : BaseModel

@end

NS_ASSUME_NONNULL_END

#import "Facility+CoreDataProperties.h"
