//
//  Facility+CoreDataProperties.m
//  
//
//  Created by AMK on 19/08/18.
//
//

#import "Facility+CoreDataProperties.h"

@implementation Facility (CoreDataProperties)

+ (NSFetchRequest<Facility *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Facility"];
}


@dynamic name;
@dynamic options;

@end
