//
//  FacilityOptionTuple+CoreDataProperties.h
//  
//
//  Created by AMK on 19/08/18.
//
//

#import "FacilityOptionTuple+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FacilityOptionTuple (CoreDataProperties)

+ (NSFetchRequest<FacilityOptionTuple *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *facilityId;
@property (nullable, nonatomic, copy) NSString *optionId;

@end

NS_ASSUME_NONNULL_END
