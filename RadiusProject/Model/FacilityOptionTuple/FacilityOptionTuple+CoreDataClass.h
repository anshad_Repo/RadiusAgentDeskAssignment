//
//  FacilityOptionTuple+CoreDataClass.h
//  
//
//  Created by AMK on 19/08/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface FacilityOptionTuple : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "FacilityOptionTuple+CoreDataProperties.h"
