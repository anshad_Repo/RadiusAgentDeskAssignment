//
//  FacilityOptionTuple+CoreDataProperties.m
//  
//
//  Created by AMK on 19/08/18.
//
//

#import "FacilityOptionTuple+CoreDataProperties.h"

@implementation FacilityOptionTuple (CoreDataProperties)

+ (NSFetchRequest<FacilityOptionTuple *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FacilityOptionTuple"];
}

@dynamic facilityId;
@dynamic optionId;

@end
