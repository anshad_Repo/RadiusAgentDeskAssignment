//
//  Option+CoreDataProperties.h
//  
//
//  Created by AMK on 19/08/18.
//
//

#import "Option+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Option (CoreDataProperties)

+ (NSFetchRequest<Option *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *icon;
@property (nullable, nonatomic, copy) NSString *name;

@end

NS_ASSUME_NONNULL_END
