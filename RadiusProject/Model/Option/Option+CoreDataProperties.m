//
//  Option+CoreDataProperties.m
//  
//
//  Created by AMK on 19/08/18.
//
//

#import "Option+CoreDataProperties.h"

@implementation Option (CoreDataProperties)

+ (NSFetchRequest<Option *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Option"];
}

@dynamic icon;
@dynamic name;

@end
