//
//  Option+CoreDataClass.h
//  
//
//  Created by AMK on 19/08/18.
//
//

#import <Foundation/Foundation.h>
#import "BaseModel+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface Option : BaseModel

@end

NS_ASSUME_NONNULL_END

#import "Option+CoreDataProperties.h"
