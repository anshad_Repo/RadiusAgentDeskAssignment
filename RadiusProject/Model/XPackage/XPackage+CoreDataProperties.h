//
//  XPackage+CoreDataProperties.h
//  
//
//  Created by AMK on 17/08/18.
//
//

#import "XPackage+CoreDataClass.h"

// 'X' in "XPackage" could be anything like Tour, Hotel .. whaterver suitable for this problem.

NS_ASSUME_NONNULL_BEGIN

@interface XPackage (CoreDataProperties)

+ (NSFetchRequest<XPackage *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSSet<Facility *> *facilities;
@property (nullable, nonatomic, retain) NSSet<ExclusionPair *> *exclusions;

@end

@interface XPackage (CoreDataGeneratedAccessors)

- (void)addFacilitiesObject:(Facility *)value;
- (void)removeFacilitiesObject:(Facility *)value;
- (void)addFacilities:(NSSet<Facility *> *)values;
- (void)removeFacilities:(NSSet<Facility *> *)values;

- (void)addExclusionsObject:(ExclusionPair *)value;
- (void)removeExclusionsObject:(ExclusionPair *)value;
- (void)addExclusions:(NSSet<ExclusionPair *> *)values;
- (void)removeExclusions:(NSSet<ExclusionPair *> *)values;

@end

NS_ASSUME_NONNULL_END
