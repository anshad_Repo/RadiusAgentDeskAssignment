//
//  XPackage+CoreDataProperties.m
//  
//
//  Created by AMK on 17/08/18.
//
//

#import "XPackage+CoreDataProperties.h"

@implementation XPackage (CoreDataProperties)

+ (NSFetchRequest<XPackage *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"XPackage"];
}

@dynamic facilities;
@dynamic exclusions;

@end
