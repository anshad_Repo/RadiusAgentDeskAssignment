//
//  XPackage+CoreDataClass.h
//  
//
//  Created by AMK on 17/08/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ExclusionPair, Facility;

NS_ASSUME_NONNULL_BEGIN

@interface XPackage : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "XPackage+CoreDataProperties.h"
