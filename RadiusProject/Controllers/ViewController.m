//
//  ViewController.m
//  RadiusProject
//
//  Created by AMK on 11/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "FacilitiesViewController.h"
#import "FacilityView.h"
#import "RadiusFacilityTableViewCell.h"
#import "FacilityOptionsInteractor.h"

@interface FacilitiesViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) FacilityView *facilitiesView;
@property (nonatomic, strong) FacilityOptionsInteractor *interactor;

@end

@implementation FacilitiesViewController

#pragma mark - View

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSubviews];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [self.interactor checkforAutoSync];
}


- (void)addSubviews{
    
    [self.view addSubview:self.facilitiesView];
    [self.facilitiesView autoPinEdgesToSuperviewEdges];
    
}

#pragma mark - Getters

- (FacilityView *)facilitiesView{
    
    if(!_facilitiesView){
        
        _facilitiesView = [FacilityView newAutoLayoutView];
        _facilitiesView.facilityTableView.dataSource = self;
        _facilitiesView.facilityTableView.delegate = self;
    }
    return _facilitiesView;
}

- (FacilityOptionsInteractor *)interactor{
    
    if(!_interactor){
        
        _interactor = [FacilityOptionsInteractor new];
    }
    return _interactor;
}



#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RadiusFacilityTableViewCell *cell = [self.facilitiesView.facilityTableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.facilityOptionLabel.text = @"Hello";
    return cell;
}



#pragma mark - Clear cache

- (void)didCompleteDownload:(NSNotification *)notification{
    
    [self clearCache];
}

- (void)clearCache{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
