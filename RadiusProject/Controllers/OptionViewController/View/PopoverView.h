//
//  PopOverView.h
//  RadiusProject
//
//  Created by AMK on 20/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopoverView : UIView

@property (nonatomic, strong) UITableView *optionTableView;
@property (nonatomic, strong) NSArray *optionList;

- (void)reloadView;

@end
