//
//  PopOverView.m
//  RadiusProject
//
//  Created by AMK on 20/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "PopoverView.h"

@interface PopoverView()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) BOOL didLayoutConstraints;


@end

@implementation PopoverView

- (UITableView *)optionTableView{
    
    if(!_optionTableView){
        
        _optionTableView = [UITableView newAutoLayoutView];
        [_optionTableView registerNib:[UINib nibWithNibName:@"OptionTableViewCell"  bundle:nil] forCellReuseIdentifier:@"optionCell"];
    }
    return _optionTableView;
}

+ (BOOL)requiresConstraintBasedLayout{
    
    return YES;
}

- (void)addSubViews{
    
    [self addSubview:self.optionTableView];
}

- (void)updateConstraints{
    
    if(!_didLayoutConstraints){
        [self addSubViews];
        [self.optionTableView autoPinEdgesToSuperviewEdges];
    }
    [super updateConstraints];
}


#pragma mark - View

- (void)reloadView{
    
    [self.optionTableView reloadData];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
