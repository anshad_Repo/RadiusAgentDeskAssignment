//
//  PopoverViewController.h
//  RadiusProject
//
//  Created by AMK on 20/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacilityViewModel.h"

@protocol OptionControllerDelegate

- (void)optionController:(id)controller option:(id)optionSelected facility:(id)facility;

@end;


@interface OptionViewController : UIViewController

@property (nonatomic, strong) FacilityViewModel *facilityVm;
@property (nonatomic, weak) id<OptionControllerDelegate> delegate;

@end
