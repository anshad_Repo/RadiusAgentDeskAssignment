//
//  PopoverViewController.m
//  RadiusProject
//
//  Created by AMK on 20/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "OptionViewController.h"
#import "PopoverView.h"
#import "OptionTableViewCell.h"
#import "OptionVM.h"


@interface OptionViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) PopoverView *popoverView;
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (nonatomic, strong) UIBarButtonItem *backButton;

@end

@implementation OptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSubViews];
    // Do any additional setup after loading the view.
}

- (void)addSubViews{
    
    [self.view addSubview:self.popoverView];
    [self.popoverView autoPinEdgesToSuperviewEdges];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}

- (void)viewWillAppear:(BOOL)animated{
    
    [self.popoverView.optionTableView reloadData];
}


#pragma mark - Getters

- (PopoverView *)popoverView{
    
    if(!_popoverView){
        
        _popoverView = [PopoverView newAutoLayoutView];
        _popoverView.optionTableView.dataSource = self;
        _popoverView.optionTableView.delegate = self;
    }
    return _popoverView;
}

- (UIBarButtonItem *)doneButton{
    
    if(!_doneButton){
        
        _doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(didClickDoneButton:)];
    }
    return _doneButton;
}



#pragma mark - Table View data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.facilityVm.options.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OptionTableViewCell *cell = [self.popoverView.optionTableView dequeueReusableCellWithIdentifier:@"optionCell" forIndexPath:indexPath];
    cell.viewModel = [self.facilityVm.options objectAtIndex:indexPath.row];
    [cell reloadData];
    return cell;
}


#pragma mark - Table view Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{

    OptionVM *optionVm = [self.facilityVm.options objectAtIndex:indexPath.row];
    
    if(!optionVm.selected){
        
        optionVm.selected = YES;
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        
        optionVm.selected = NO;
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    }
    
    [self.delegate optionController:self option:optionVm facility:self.facilityVm];
}

#pragma mark - Button events

- (void)didClickDoneButton:(id)button{
    
    
    [self.navigationController popViewControllerAnimated:YES];
}
                       
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
