//
//  OptionTableViewCell.m
//  RadiusProject
//
//  Created by AMK on 20/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "OptionTableViewCell.h"
#import "UIColor+RADAdditions.h"

@implementation OptionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];

    // Configure the view for the selected state
}

#pragma mark - reload

- (void)reloadData{
    
    [self.optionImageView setImage:[UIImage imageNamed:self.viewModel.icon]];
    self.optionLabel.text = self.viewModel.name;
    
    //Only avalable options are allowed for selection.
    if(!self.viewModel.available){
        
        self.backgroundColor = [UIColor disabledGrayColor];
        self.userInteractionEnabled = NO;
    }else{
        
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
    }
    
    //Selection mark
    if(self.viewModel.selected){
        
        self.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
