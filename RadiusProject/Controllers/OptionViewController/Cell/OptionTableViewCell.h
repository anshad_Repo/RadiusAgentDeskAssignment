//
//  OptionTableViewCell.h
//  RadiusProject
//
//  Created by AMK on 20/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionVM.h"

@interface OptionTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *optionImageView;
@property (strong, nonatomic) IBOutlet UILabel *optionLabel;
@property (strong, nonatomic) OptionVM *viewModel;

- (void)reloadData;

@end
