//
//  ViewController.m
//  RadiusProject
//
//  Created by AMK on 11/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "FacilitiesViewController.h"
#import "FacilityView.h"
#import "RadiusFacilityTableViewCell.h"
#import "FacilityOptionsInteractor.h"
#import "OptionViewController.h"
#import "PopoverView.h"
#import "FacilityViewModel.h"
#import "ExclusionPairVM.h"
#import "OptionVM.h"
#import "OptionViewController.h"

@interface FacilitiesViewController ()<UITableViewDelegate, UITableViewDataSource, OptionControllerDelegate>

@property (nonatomic, strong) FacilityView *facilitiesView;
@property (nonatomic, strong) FacilityOptionsInteractor *interactor;
@property (nonatomic, strong) NSArray *facilities;
@property (nonatomic, strong) NSArray *exclusions;
@property (nonatomic, strong) OptionViewController *optionController;
@property (nonatomic, strong) NSMutableArray *selectedItems;

@end

@implementation FacilitiesViewController

#pragma mark - View

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSubviews];
    // Do any additional setup after loading the view, typically from a nib.
}



- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    if([self.interactor autoSyncIfNeeded]){
        
        [SVProgressHUD showWithStatus:@"Please wait"];
        [self.interactor downloadData:^(NSError *error) {
            
            [SVProgressHUD dismiss];
        }];
    }
}


- (void)viewDidAppear:(BOOL)animated{
    
}

- (void)addSubviews{
    
    [self.view addSubview:self.facilitiesView];
    [self.facilitiesView autoPinEdgesToSuperviewEdges];
}

#pragma mark - Getters

- (FacilityView *)facilitiesView{
    
    if(!_facilitiesView){
        
        _facilitiesView = [FacilityView newAutoLayoutView];
        _facilitiesView.facilityTableView.dataSource = self;
        _facilitiesView.facilityTableView.delegate = self;
    }
    return _facilitiesView;
}

- (FacilityOptionsInteractor *)interactor{
    
    if(!_interactor){
        
        _interactor = [FacilityOptionsInteractor new];
    }
    return _interactor;
}


- (NSArray *)facilities{
    
    if(!_facilities){
        
        _facilities = [self.interactor getFacilitiesViewModels];
    }
    return _facilities;
}

- (NSArray *)exclusions{
    
    if(!_exclusions){
        
        _exclusions = [self.interactor getExclusionGroupsViewModels];
    }
    return _exclusions;
}

- (OptionViewController *)optionController{
    
    if(!_optionController){
        
        _optionController = [OptionViewController new];
        _optionController.delegate = self;
    }
    return _optionController;
}

- (NSMutableArray *)selectedItems{
    
    if(!_selectedItems){
        
        _selectedItems = [NSMutableArray new];
    }
    return _selectedItems;
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.facilities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RadiusFacilityTableViewCell *cell = [self.facilitiesView.facilityTableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    FacilityViewModel *facilityVm = [self.facilities objectAtIndex:indexPath.row];
    cell.facilityNameLabel.text = facilityVm.name;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70.0f;
}

#pragma mark - TableView delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FacilityViewModel *facilityVm = [self.facilities objectAtIndex:indexPath.row];
    self.optionController.facilityVm = facilityVm;
    [self.navigationController pushViewController:self.optionController animated:YES];
    
}


#pragma mark - OptionController delegate

- (void)optionController:(id)controller option:(id)optionSelected facility:(id)facility{
    
    /*
     Find out current selected options into selectedItems-array
     */
    [self.selectedItems removeAllObjects];
    
    for(FacilityViewModel *facilityVm in self.facilities){
        
        [self.selectedItems addObjectsFromArray:[facilityVm.options filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            
            OptionVM *optionVm = (OptionVM *)evaluatedObject;
            
            //Reset availability
            optionVm.available = YES;
            return optionVm.selected;
        }]]];
    }
    
    
    //Update the availability of options with selected Options
    for(OptionVM *selectedOptionVm in self.selectedItems){
        
        for(ExclusionPairVM *exclusionPair in self.exclusions){
            
            //Find out this selected Item is a part of this exclusion pair
            FacilityOptionVM *itemFound = [[exclusionPair.mutualExclusionItems filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:
                                                                                            @"facility_id == %@ AND option_id == %@",
                                                                                             selectedOptionVm.facilityId,selectedOptionVm.identifier]] firstObject];
            
            if(itemFound){
                
                for(FacilityOptionVM *facOption in exclusionPair.mutualExclusionItems){
                    
                    //Update availability of the other items in the pair as NO.
                    FacilityViewModel *facility = [[self.facilities filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:
                                                                                                 @"identifier == %@ AND identifier != %@",
                                                                                                 facOption.facility_id,itemFound.facility_id]] firstObject];
                    OptionVM *otherOptionVmInPair = [[facility.options filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:
                                                                                         @"identifier == %@ AND identifier != %@",
                                                                                         facOption.option_id,itemFound.option_id]] firstObject];
                    otherOptionVmInPair.available = NO;
                }
            }
        }
    }
}

#pragma mark - Clear cache

- (void)didCompleteDownload:(NSNotification *)notification{
    
    [self clearCache];
}

- (void)clearCache{
    
    self.facilities = nil;
    self.exclusions = nil;
    [self.facilitiesView reloadView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
