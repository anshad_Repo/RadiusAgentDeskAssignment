//
//  FacilityOptionsInteractor.m
//  RadiusProject
//
//  Created by AMK on 11/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "FacilityOptionsInteractor.h"
#import "Facility+CoreDataClass.h"
#import "ExclusionPair+CoreDataClass.h"
#import "FacilityOptionTuple+CoreDataClass.h"
#import "Option+CoreDataClass.h"
#import "BaseModel+CoreDataClass.h"
#import "Facility+CoreDataClass.h"
#import "CoreDataAccessManager.h"
//#import "XPackage+CoreDataClass.h"
#import "NSManagedObject+RADAdditions.h"

#import "FacilityViewModel.h"
#import "ExclusionPairVM.h"

@interface FacilityOptionsInteractor()

@property (nonatomic, strong) NSArray *facilities;
@property (nonatomic, strong) NSArray *exclusions;

@end

@implementation FacilityOptionsInteractor


#pragma mark - Getters

- (NSArray *)facilities{
    
    if(!_facilities){
        
        _facilities = [self.dataManager getFacilitiesList];
    }return _facilities;
}

- (NSArray *)exclusions{
    
    if(!_exclusions){
        
        _exclusions = [self.dataManager getExclusionsList];
    }
    return _exclusions;
}


- (NSArray *)getFacilitiesViewModels{
    
    NSMutableArray *facilitiesViewModels = [NSMutableArray new];
    for(Facility *facility in self.facilities){
        
        FacilityViewModel *facilityViewModel = [FacilityViewModel new];
        facilityViewModel.name = facility.name;
        facilityViewModel.identifier = facility.identifier;
        for(Option *option in facility.options){
            
            OptionVM *optionVm = [OptionVM new];
            optionVm.identifier = option.identifier;
            optionVm.name = option.name;
            optionVm.icon = option.icon;
            optionVm.available = YES;
            optionVm.facilityId = facility.identifier;
            optionVm.facilityName = facility.name;
            [facilityViewModel.options addObject:optionVm];
        }
        [facilitiesViewModels addObject:facilityViewModel];
    }
    return facilitiesViewModels;
}


- (NSArray *)getExclusionGroupsViewModels{
    
    NSMutableArray *exclusionPairs = [NSMutableArray new];
    for(ExclusionPair *exclusionPair in self.exclusions){
        
        ExclusionPairVM *exclusionPairVm = [ExclusionPairVM new];
        for(FacilityOptionTuple *facilityOption in exclusionPair.mutualExclusionPair){
            
            FacilityOptionVM *facilityOptionVm = [FacilityOptionVM new];
            facilityOptionVm.facility_id = facilityOption.facilityId;
            facilityOptionVm.option_id = facilityOption.optionId;
            [exclusionPairVm.mutualExclusionItems addObject:facilityOptionVm];
        }
        [exclusionPairs addObject:exclusionPairVm];
    }
    return exclusionPairs;
}

#pragma mark - Relead cache

- (void)didCompleteDownload:(NSNotification *)notification{
    
    [self clearCache];
}

- (void)clearCache{
    
    [super clearCache];
    self.facilities = nil;
    self.exclusions = nil;
}

@end
