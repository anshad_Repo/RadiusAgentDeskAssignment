//
//  FacilityOptionsInteractor.h
//  RadiusProject
//
//  Created by AMK on 11/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseInteractor.h"

@interface FacilityOptionsInteractor : BaseInteractor


- (NSArray *)getFacilitiesViewModels;

- (NSArray *)getExclusionGroupsViewModels;

- (void)getExclusions;

@end
