
//
//  XViewController.m
//  RadiusProject
//
//  Created by AMK on 19/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "XViewController.h"
#import "XViewInteractor.h"
#import "FacilitiesViewController.h"


@interface XViewController ()

@property (nonatomic, strong) XViewInteractor *interactor;

@end

@implementation XViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    
    if([self.interactor autoSyncIfNeeded]){
        
        [SVProgressHUD showWithStatus:@"Please wait"];
        [self.interactor downloadData:^(NSError *error) {
            
            [SVProgressHUD dismiss];
            FacilitiesViewController *facilityViewController = [FacilitiesViewController new];
            [self.navigationController pushViewController:facilityViewController animated:YES];
        }];
    }else{
        
        FacilitiesViewController *facilityViewController = [FacilitiesViewController new];
        [self.navigationController pushViewController:facilityViewController animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Getters

- (XViewInteractor *)interactor{
    
    if(!_interactor){
        
        _interactor = [XViewInteractor new];
    }
    return _interactor;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

