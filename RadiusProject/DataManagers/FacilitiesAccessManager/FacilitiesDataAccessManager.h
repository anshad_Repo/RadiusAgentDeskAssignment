//
//  FacilitiesAccessManager.h
//  RadiusProject
//
//  Created by AMK on 17/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FacilitiesDataAccessManager : NSObject

+ (instancetype)defaultManager;

/*
 Return facility list contains multiple options.
 */
- (NSArray *)facilitiesList;

/*
 Return exclusion list contains groups of different options which cannot be selected together from different facilities
 */
- (NSArray *)exclusionsList;


- (void)saveData:(NSDictionary *)facilitiesData error:(NSError **)error;


@end
