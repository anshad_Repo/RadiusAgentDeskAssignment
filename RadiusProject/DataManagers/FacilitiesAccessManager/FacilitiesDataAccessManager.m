//
//  FacilitiesAccessManager.m
//  RadiusProject
//
//  Created by AMK on 17/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "FacilitiesDataAccessManager.h"
#import "Facility+CoreDataClass.h"
#import "ExclusionPair+CoreDataClass.h"
#import "FacilityOptionTuple+CoreDataClass.h"
#import "Option+CoreDataClass.h"
#import "BaseModel+CoreDataClass.h"
#import "Facility+CoreDataClass.h"
#import "CoreDataAccessManager.h"
//#import "XPackage+CoreDataClass.h"
#import "NSManagedObject+RADAdditions.h"

NSString *const kFAMFacilities = @"facilities";
NSString *const kFAMExclusions = @"exclusions";

@interface FacilitiesDataAccessManager()

@property (nonatomic, strong) NSFetchRequest *facilityFetchRequest;
@property (nonatomic, strong) NSFetchRequest *exclusionFetchRequest;
@property (nonatomic, strong) CoreDataAccessManager *coreDataManager;
@property (nonatomic, strong) dispatch_queue_t coreDataSerialQue;

@end

@implementation FacilitiesDataAccessManager

+ (instancetype)defaultManager{
    
    static FacilitiesDataAccessManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
    
        sharedManager = [[self class] new];
        sharedManager.coreDataManager = [CoreDataAccessManager sharedInstance];
        
        //Fetch requests
        sharedManager.facilityFetchRequest = [Facility fetchRequest];
        sharedManager.exclusionFetchRequest = [ExclusionPair fetchRequest];
        sharedManager.coreDataSerialQue = dispatch_queue_create("RAD.coredata.que", DISPATCH_QUEUE_SERIAL);
    });
    return sharedManager;
}


- (NSArray *)facilitiesList{
    
    return  [self.coreDataManager.managedObjectContext executeFetchRequest:self.facilityFetchRequest error:nil];
    
}

- (NSArray *)exclusionsList{
    
    return [self.coreDataManager.managedObjectContext executeFetchRequest:self.exclusionFetchRequest error:nil];
}

- (void)saveData:(NSDictionary *)facilitiesData error:(NSError *__autoreleasing *)error{
    
    
    dispatch_sync(self.coreDataSerialQue, ^{
        
        NSArray *facilities = [facilitiesData valueForKey:kFAMFacilities];
        NSArray *exclusions = [facilitiesData valueForKey:kFAMExclusions];
        [self saveFacilities:facilities];
        [self saveExclusions:exclusions];
        [self.coreDataManager.managedObjectContext save:nil];
    });
    
}


/*
   Facilities save/merge. As Id is supplied for Facility as well as Option Save/Merging is possible
 */
- (void)saveFacilities:(NSArray *)facilities{
    
    //Facilities section - JSON Deserialization
    for(NSDictionary *facilityDict in facilities){
        
        Facility *facility = nil;
        NSString *facilityId = [facilityDict valueForKey:@"facility_id"];
        
        //Fetch Request Fecility
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Facility" inManagedObjectContext:
                                                                self.coreDataManager.managedObjectContext];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier like %@",facilityId];
        [fetchRequest setEntity:entity];
        [fetchRequest setPredicate:predicate];
        
        
        NSError *error;
        NSArray *items = [self.coreDataManager.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        if(items.count > 1){
            
            NSLog(@"%@",@"Error Duplicate items for Facility. Merge conflicts.");
        }else if(items.count == 1){
            
            //Facility Merge
            facility = [items firstObject];
        }else{
            
            //Facility Create
            facility = [NSEntityDescription insertNewObjectForEntityForName:@"Facility" inManagedObjectContext:
                        self.coreDataManager.managedObjectContext];
        }
        facility.identifier = facilityId;
        facility.name = [facilityDict valueForKey:@"name"];
        NSArray *options = [facilityDict valueForKey:@"options"];
        NSMutableSet *optionsSet = [NSMutableSet new];
        for(NSDictionary *optionDict in options){
            
                Option *option = nil;
                NSString *optionId = [optionDict valueForKey:@"id"];
            
                //Fetch request - Option
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Option" inManagedObjectContext:
                                               self.coreDataManager.managedObjectContext];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier like %@",optionId];
                [fetchRequest setEntity:entity];
                [fetchRequest setPredicate:predicate];
            
            
                NSError *error;
                NSArray *items = [self.coreDataManager.managedObjectContext executeFetchRequest:fetchRequest error:&error];
                if(items.count > 1){
                    
                    NSLog(@"%@",@"Error Duplicate items for Facility. Merge conflicts.");
                    return;
                }else if(items.count == 1){
                    
                    //Option Merge
                    option = [items firstObject];
                }else{
                    
                    //Option Creation
                    option = [NSEntityDescription insertNewObjectForEntityForName:@"Option" inManagedObjectContext:
                                                                            self.coreDataManager.managedObjectContext];
                }
                option.identifier = optionId;
                option.name = [optionDict valueForKey:@"name"];
                option.icon = [optionDict valueForKey:@"icon"];
            
                [optionsSet addObject:option];
        }
        [facility addOptions:optionsSet];
    }
    
}


/*
 Exclusions data save. Merging is Not Possible as Id is not specified in API;
 
 */
- (void)saveExclusions:(NSArray *)exclusions{
    
    //Exclusion section - JSON Deserialization
    for(NSArray *exclusionPairArray in exclusions){
    
        
        NSMutableSet *exclusionPairSet = [NSMutableSet new];
        for (NSDictionary *facilityOptionTupleDict in exclusionPairArray){
            
            FacilityOptionTuple *facilityOptionTuple = [NSEntityDescription insertNewObjectForEntityForName:@"FacilityOptionTuple"
                                                                                     inManagedObjectContext:self.coreDataManager.managedObjectContext];
            facilityOptionTuple.facilityId = [facilityOptionTupleDict valueForKey:@"facility_id"];
            facilityOptionTuple.optionId = [facilityOptionTupleDict valueForKey:@"options_id"];
            [exclusionPairSet addObject:facilityOptionTuple];
        }
        ExclusionPair *exclusionPair = [NSEntityDescription insertNewObjectForEntityForName:@"ExclusionPair" inManagedObjectContext:
                                        self.coreDataManager.managedObjectContext];
        [exclusionPair addMutualExclusionPair:exclusionPairSet];
    }
    
}

@end
