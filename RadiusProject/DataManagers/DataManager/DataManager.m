//
//  DataManager.m
//  RadiusProject
//
//  Created by AMK on 17/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "DataManager.h"
#import "FacilitiesDataAccessManager.h"

NSString *const kNotificationOnDownloadCompletion = @"DownloadCompleted";

@interface DataManager()

@property (nonatomic, strong) FacilitiesDataAccessManager *facilitiesDataAccessManager;

@end

@implementation DataManager

+ (instancetype)defaultManager{
    
    static DataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedManager = [DataManager new];
        sharedManager.facilitiesDataAccessManager = [FacilitiesDataAccessManager defaultManager];
    });
    return sharedManager;
}

#pragma mark - read data

- (NSArray *)getFacilitiesList{

    NSArray *facilities = [self.facilitiesDataAccessManager facilitiesList];
    if(facilities)
        return facilities;
    else
        return [NSArray new];
}

- (NSArray *)getExclusionsList{
    
    NSArray *exclusionList = [self.facilitiesDataAccessManager exclusionsList];
    if(exclusionList)
        return exclusionList;
    else
        return [NSArray new];
}



#pragma mark - write data

- (void)saveFacilitiesData:(id)facilityData error:(NSError **)error{
    
    [self.facilitiesDataAccessManager saveData:facilityData error:error];
    
    if(!*error){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOnDownloadCompletion
                                                                                        object:nil];
    }
}

@end
