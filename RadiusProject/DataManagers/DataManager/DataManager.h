//
//  DataManager.h
//  RadiusProject
//
//  Created by AMK on 17/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Facility+CoreDataClass.h"

extern NSString *const kNotificationOnDownloadCompletion;
@interface DataManager : NSObject

+ (instancetype)defaultManager;

/*
 Return facility list contains options.
 */
- (NSArray *)getFacilitiesList;

/*
 Return exclusion list indicates different items which cannot be selected together from different facilities
 */
- (NSArray *)getExclusionsList;

- (void)saveFacilitiesData:(id )facilityData error:(NSError **)error;

@end
