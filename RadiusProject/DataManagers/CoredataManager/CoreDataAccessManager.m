//
//  CoreDataManager.m
//  RadiusProject
//
//  Created by AMK on 11/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "CoreDataAccessManager.h"
#import <CoreData/CoreData.h>

@interface CoreDataAccessManager ()
@property (nonatomic, strong, readwrite) NSManagedObjectContext *managedObjectContext;

@end
@implementation CoreDataAccessManager

+ (CoreDataAccessManager *) sharedInstance{
    static CoreDataAccessManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CoreDataAccessManager alloc] init];
    });
    return sharedInstance;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext == nil){
        
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:[self persistenceStoreCoordinator]];
    }
    return _managedObjectContext;
}

- (NSPersistentStoreCoordinator *)persistenceStoreCoordinator {
    
    //Create Managed object model which gives the DB model
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:url];
    
    //Create a persistence co-ordinator and associate that with the Managed Object Model
    NSPersistentStoreCoordinator *storeCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    
    //add the persistence store to co-odinator
    NSError *error;
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"mydbstore.sqlite"];
    
    NSPersistentStore *store = [storeCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
    
    if (store == nil) {
        NSLog(@"adding persistence store faileed - %@", [error localizedDescription]);
    }
    
    return storeCoordinator;
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end

