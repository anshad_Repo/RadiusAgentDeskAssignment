//
//  CoreDataManager.h
//  RadiusProject
//
//  Created by AMK on 11/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataAccessManager : NSObject

+(CoreDataAccessManager *) sharedInstance;

- (NSManagedObjectContext *)managedObjectContext ;

@end
