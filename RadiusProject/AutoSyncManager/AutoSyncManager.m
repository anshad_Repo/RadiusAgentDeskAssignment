//
//  AutoSyncManager.m
//  RadiusProject
//
//  Created by AMK on 17/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "AutoSyncManager.h"

NSString *const kASMLastSyncTime = @"lastSyncTime";
NSString *const kASMNotificationAutoSyncTrigger = @"AutoSyncTrigger";

@implementation AutoSyncManager

+ (instancetype)defaultManager{
    
    static AutoSyncManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedManager = [[self class] new];
    });
    return sharedManager;
}

- (BOOL)requiredAutoSync{
    
    NSString *lastSyncDate = [[NSUserDefaults standardUserDefaults] valueForKey:kASMLastSyncTime];
    if(lastSyncDate.length){
        
        NSDate *lastDate = [[NSDateFormatter defaultDateFormatter] dateFromString:lastSyncDate];
        if([[NSDate date] timeIntervalSinceDate:lastDate]>86400){
            //86400 24 hr in to seconds
            NSString *updatedLastSyncDate = [[NSDateFormatter defaultDateFormatter] stringFromDate:[NSDate date]];
            [[NSUserDefaults standardUserDefaults] setObject:updatedLastSyncDate forKey:kASMLastSyncTime];
            return YES;
            
        }
        return NO;
    }else{
        
        NSString *updatedLastSyncDate = [[NSDateFormatter defaultDateFormatter] stringFromDate:[NSDate date]];
        [[NSUserDefaults standardUserDefaults] setObject:updatedLastSyncDate forKey:kASMLastSyncTime];
        return YES;
    }
}

@end
