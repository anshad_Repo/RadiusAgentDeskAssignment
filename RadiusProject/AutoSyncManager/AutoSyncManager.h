//
//  AutoSyncManager.h
//  RadiusProject
//
//  Created by AMK on 17/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kASMNotificationAutoSyncTrigger;

@interface AutoSyncManager : NSObject

+ (instancetype)defaultManager;

- (BOOL)requiredAutoSync;

@end
