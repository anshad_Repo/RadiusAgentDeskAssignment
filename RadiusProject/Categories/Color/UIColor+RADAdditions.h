//
//  UIColor+RADAdditions.h
//  RadiusProject
//
//  Created by AMK on 02/09/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RADAdditions)

+ (UIColor *)disabledGrayColor;

@end
