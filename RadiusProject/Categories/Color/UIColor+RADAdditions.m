//
//  UIColor+RADAdditions.m
//  RadiusProject
//
//  Created by AMK on 02/09/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "UIColor+RADAdditions.h"

@implementation UIColor (RADAdditions)

+ (UIColor *)disabledGrayColor{
    
    return [UIColor colorWithRed:0.863 green:0.863 blue:0.863 alpha:1.00];
}

@end
