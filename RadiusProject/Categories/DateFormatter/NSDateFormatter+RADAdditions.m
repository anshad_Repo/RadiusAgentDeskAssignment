//
//  NSDateFormatter+RADAdditions.m
//  RadiusProject
//
//  Created by AMK on 20/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "NSDateFormatter+RADAdditions.h"

NSString * const kRDateFormatDefault = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";

@implementation NSDateFormatter (RADAdditions)

+ (NSDateFormatter *)defaultDateFormatter{
    
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = kRDateFormatDefault;
    });
    return dateFormatter;
}

@end
