//
//  NSDateFormatter+RADAdditions.h
//  RadiusProject
//
//  Created by AMK on 20/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kRDateFormatDefault;

@interface NSDateFormatter (RADAdditions)

+ (NSDateFormatter *)defaultDateFormatter;

@end
