//
//  NSManagedObject+RADAdditions.h
//  RadiusProject
//
//  Created by AMK on 17/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (RADAdditions)

/* avoid usage of string Constants/EntityNames
 */
+ (NSString *)entityName;

@end
