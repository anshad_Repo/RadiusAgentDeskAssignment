//
//  BaseInteractor.m
//  RadiusProject
//
//  Created by AMK on 19/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "BaseInteractor.h"


@interface BaseInteractor()


@end

@implementation BaseInteractor


#pragma mark - init

- (instancetype)init{
    
    self = [super init];
    if(self){
        
        self.dataManager = [DataManager defaultManager];
        self.networkManager = [NetworkManager defaultManager];
        self.autoSyncManager = [AutoSyncManager defaultManager];
        [self registerCustomNotifications];
        
    }
    return self;
}


#pragma mark - Notifications
- (void)registerCustomNotifications{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didCompleteDownload:)
                                                 name:kNotificationOnDownloadCompletion
                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(didRecieveAutoSyncNotification:)
//                                                 name:kASMNotificationAutoSyncTrigger
//                                               object:nil];
}

- (void)didCompleteDownload:(NSNotification *)notification{
    
    //implement this method in subclasses
}

//- (void)didRecieveAutoSyncNotification:(NSNotification *)notification{
//
//    [self downloadData:^(NSError *error) {
//        NSLog(@"%@",error.localizedDescription);
//    }];
//}


#pragma mark - Autosync

- (BOOL)autoSyncIfNeeded{
    
    return [self.autoSyncManager requiredAutoSync];
}


#pragma mark - Network calls


- (void)downloadData:(void (^)(NSError *error))completionBlock{
    
    @weakify(self);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        
        @strongify(self);
        [self.networkManager downloadFaclilityData:^(id responseObject, NSError *error) {
            
            NSError *err = nil;
            [self.dataManager saveFacilitiesData:responseObject error:&err];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(completionBlock) completionBlock(err);
            });
            
        }];
    });
}




#pragma mark - Relead cache

- (void)clearCache{
    
    //perform any common clear operation here
}

@end
