//
//  BaseInteractor.h
//  RadiusProject
//
//  Created by AMK on 19/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"
#import "NetworkManager.h"
#import "AutoSyncManager.h"


@interface BaseInteractor : NSObject

@property(nonatomic, strong) DataManager *dataManager;
@property(nonatomic, strong) NetworkManager *networkManager;
@property(nonatomic, strong) AutoSyncManager *autoSyncManager;


- (void)downloadData:(void(^)(NSError *error))completionBlock;

- (BOOL)autoSyncIfNeeded;

- (void)clearCache;

@end
