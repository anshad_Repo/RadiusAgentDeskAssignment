//
//  FacilityViewModel.h
//  RadiusProject
//
//  Created by AMK on 19/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OptionVM.h"
#import "FacilityViewModel.h"

@interface FacilityViewModel : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableArray<OptionVM *> *options;

@end
