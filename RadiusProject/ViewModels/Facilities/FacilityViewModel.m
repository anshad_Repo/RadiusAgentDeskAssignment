//
//  FacilityViewModel.m
//  RadiusProject
//
//  Created by AMK on 19/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "FacilityViewModel.h"

@implementation FacilityViewModel

- (NSMutableArray<OptionVM *> *)options{
    
    if(!_options){
        
        _options = [NSMutableArray<OptionVM *> new];
    }
    return _options;
}

@end
