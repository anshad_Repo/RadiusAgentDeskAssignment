//
//  Option.h
//  RadiusProject
//
//  Created by AMK on 19/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OptionVM : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) BOOL available;
@property (nonatomic, assign) BOOL selected;
@property (nonatomic, strong) NSString *facilityId;
@property (nonatomic, strong) NSString *facilityName;
@end
