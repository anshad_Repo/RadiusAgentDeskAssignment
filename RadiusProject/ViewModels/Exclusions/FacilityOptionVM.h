//
//  FacilityOptionVM.h
//  RadiusProject
//
//  Created by AMK on 19/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FacilityOptionVM : NSObject

@property (nonatomic, strong) NSString *facility_id;
@property (nonatomic, strong) NSString *option_id;

@end
