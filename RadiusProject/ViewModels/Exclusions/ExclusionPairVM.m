//
//  ExclusionPairVM.m
//  RadiusProject
//
//  Created by AMK on 19/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import "ExclusionPairVM.h"

@implementation ExclusionPairVM

- (NSMutableArray<FacilityOptionVM *> *)mutualExclusionItems{
    
    if(!_mutualExclusionItems){
        
        _mutualExclusionItems = [NSMutableArray<FacilityOptionVM *> new];
    } return _mutualExclusionItems;
}

@end
