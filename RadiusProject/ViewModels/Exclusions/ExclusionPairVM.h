//
//  ExclusionPairVM.h
//  RadiusProject
//
//  Created by AMK on 19/08/18.
//  Copyright © 2018 ANSHAD M K. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FacilityOptionVM.h"

@interface ExclusionPairVM : NSObject

//More than 2 mutualExcludeditems consired in a group - so it would be an array insted of just two options
@property (nonatomic, strong) NSMutableArray<FacilityOptionVM *> *mutualExclusionItems;

@end
